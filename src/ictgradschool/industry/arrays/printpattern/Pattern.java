package ictgradschool.industry.arrays.printpattern;

public class Pattern<s> {
    private char b;
    private int number;



    public Pattern(int number, char b)
    {
        this.number = number;
        this.b = b;

    }

    @Override
    public String toString() {
        String s="";
        for (int i = number; i > 0; i--) {
            s += b;
        }
        return s;
    }

    public int getNumberOfCharacters() {
        return number;
    }

    public void setNumberOfCharacters(int number) {
        this.number = number;
    }
}

