package ictgradschool.industry.arrays.mobilephones;

public class MobilePhone {

    // TODO Declare the 3 instance variables:
    private String brand;
    private String model;
    private double price;
    
    public MobilePhone(String brand, String model, double price)
    {
        this.brand = brand;
        this.model=model;
        this.price=price;
        // Complete this constructor method
    }

    // TODO Uncomment these methods once the corresponding instance variable has been declared.
    public String getBrand() {
        return brand;
    }
    
    public void setBrand(String brand) {
        this.brand = brand;
    }
    
    // TODO Insert getModel() method here
    public String getModel()
    {
        return this.model;
    }
    
    // TODO Insert setModel() method here
    public void setModel(String model)
    {
        this.model = model;
    }
    // TODO Insert getPrice() method here
    public double getPrice()
    {
        return this.price;
    }
    
    // TODO Insert setPrice() method here
    public void setPrice(double price)
    {
        this.price = price;
    }
    
    // TODO Insert toString() method here

    @Override
    public String toString() {
        String s="";
        s = brand +" "+ model +" which cost "+price;
        return s;
    }


    // TODO Insert isCheaperThan() method here
    public boolean isCheaperThan(MobilePhone obj)
    {
        return this.price<obj.price;
    }
    
    // TODO Insert equals() method here
    public boolean equals(MobilePhone obj)
    {
        return (this.model.equals(obj.model)&& this.brand.equals(obj.brand));

    }

}


